// Dependencies
const { port, portSecure } = require('./config');
const http = require('http');
const querystring = require('querystring');
const { URL } = require('url');
const { StringDecoder } = require('string_decoder');
const fs = require('fs');
const handlers = require('./lib/handlers');

const serverHandler = async (req, res) => {
  const data = await parseReq(req);
  const { method, path, query, headers, payload } = data;
  // Find matching handler for requested path
  const handler = findHandler(path);

  let parsedPayload;
  try {
    parsedPayload = JSON.parse(payload || '{}');
  } catch (e) {
    // Answer to the client
    // Set the response type header. We will always return JSON.
    res.setHeader('Content-Type', 'application/json');
    // Write the response status code
    res.writeHead(400);
    // Write the handler output payload
    return res.end(JSON.stringify({
      message: 'HTTP body must be valid JSON'
    }));
  }

  data.payload = parsedPayload;
  // Call the handler
  const handlerOutput = await handler(data);

  // Answer to the client
  // Set the response type header. We will always return JSON.
  res.setHeader('Content-Type', 'application/json');
  // Write the response status code
  res.writeHead(handlerOutput.statusCode || 200);
  // Write the handler output payload
  res.end(JSON.stringify(handlerOutput.payload || {}));

  // Log the requested path
  console.info(`[${method}] @ ${path} with payload: ${payload}`);
  console.table(query);
  console.table(headers);
};

function parseReq (req) {
  return new Promise((resolve, reject) => {
    // Get the requested URL and parse it
    const parsedUrl = new URL(req.url, 'dummy://');

    // Extract the path from the URL
    const path = parsedUrl.pathname.replace(/^\/+|\/+$/g, '');

    // Get the query string as an object
    const query = querystring.decode(req.url.substring(req.url.indexOf('?') + 1));

    // Get the HTTP method
    const method = req.method.toLowerCase();

    // Get the HTTP headers
    const headers = req.headers;

    // Get the payload if any found
    const decoder = new StringDecoder('utf8');
    let payload = '';
    req.on('data', (data) => {
    // Collect the payload pieces and merge them into one
      payload += decoder.write(data);
    });
    req.on('end', () => {
    // End the collection of the payload
      payload += decoder.end();

      // After payload decoding is done, process the request

      // Resolve the promise
      resolve({
        path,
        query,
        method,
        headers,
        payload
      });
    });
  });
}

/**
 * Handler definition
 * @typedef {Object} Handler
 * @property {Number} statusCode - HTTP status code of the answer
 * @property {Object} payload - Answer for the client's request
 */

/**
 * Looks up the handler for requested path or returns the default,
 * 404 Not Found one.
 *
 * @param {String} path
 * @returns {Promise<Handler>}
 */
function findHandler (path = '') {
  return handlers[path] ? handlers[path] : handlers.notFound;
}

// Create a new instance of a HTTP server
const server = http.createServer(serverHandler);
// Listen for the desired HTTP port from config
server.listen(port, () => {
  console.log(`The server is listening on port ${port}`);
});

// Create a new instance of a HTTPS server
const serverSecure = http.createServer(serverHandler);
// Listen for the desired HTTPS port from config
serverSecure.listen({
  key: fs.readFileSync('./https/key.pem'),
  cert: fs.readFileSync('./https/cert.pem'),
  port: portSecure
}, () => {
  console.log(`The secure server is listening on port ${portSecure}`);
});
