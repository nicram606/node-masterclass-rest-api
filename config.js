// Container for all environments
const environments = {
  staging: {
    port: 3000,
    portSecure: 3003,
    envName: 'staging',
    hashingSecret: 'longHashSecret',
    maxChecksPerUser: 5
  },
  production: {
    port: 5000,
    portSecure: 5005,
    envName: 'production',
    hashingSecret: 'longHashSecretForProduction',
    maxChecksPerUser: 5
  }
};

// Determine which environment was passed as a command-line argument
const currentEnv = process.env.NODE_ENV ? process.env.NODE_ENV.toLowerCase() : 'staging';

if (!process.env.NODE_ENV) {
  console.warn('No NODE_ENV environment variable found, defaulting to staging...');
}

if (!environments[currentEnv]) {
  throw new Error(`Invalid NODE_ENV=${process.env.NODE_ENV}. Proper values are ${Object.keys(environments)}.`);
}

module.exports = environments[currentEnv];
