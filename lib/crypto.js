const crypto = require('crypto');
const assert = require('assert');
const config = require('../config');

/**
 * @param {String} string Text to be hashed
 * @returns {String} hashed text
 */
function hash (string) {
  if (!(typeof string === 'string' && string.length > 0)) {
    throw new Error('Cannot hash empty string');
  }

  return crypto.createHmac('sha256', config.hashingSecret).update(string).digest('hex');
}

const possibleChars = 'qwertyuiopasdfghjklzxcvbnm1234567890';
/**
 * Generates random string with given @param length.
 *
 * Uniqueness is guaranteed by the randomness of the system,
 * thus bug lengths are more likely to be unique.
 *
 * @param {Number} length length of desired string
 * @returns {String}
 */
function uniqueString (length = 32, chars = possibleChars) {
  assert.strictEqual(typeof length === 'number' && length > 0, true);
  return new Array(length).fill('').map((_) => possibleChars.charAt(Math.floor(Math.random() * possibleChars.length))).join('');
}

module.exports = {
  hash,
  uniqueString
};
