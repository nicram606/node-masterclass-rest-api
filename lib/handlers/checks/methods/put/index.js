const { validateFields, validateQuery } = require('./validation');
const { verifyToken } = require('../../../../auth');
const { updateUser } = require('./data');

// Required data: (query) phone
// Optional data: firstName, lastName, password - at least one must be specified
// TODO: only let authed user update their object
module.exports = async function putUserHandler (data) {
  const sanitizedPayload = sanitizePayload(data.payload);
  const validFields = Object.values(sanitizedPayload).filter((fieldContent) => fieldContent);
  if (validFields.length === 0) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid payload. At least one of the fields: firstName (string), lastName (string), password (string) are required.'
      }
    };
  }

  const { isValid, reasons } = validateFields(sanitizedPayload, 2);
  if (!isValid) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid payload',
        reasons
      }
    };
  }

  const sanitizedQuery = sanitizeQuery(data.query);
  if (Object.values(sanitizedQuery).some((fieldContent) => !fieldContent)) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid query. Field phone (string) is required.'
      }
    };
  }

  const queryValidation = validateQuery(sanitizedQuery);
  if (!queryValidation.isValid) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid query',
        reasons: queryValidation.reasons
      }
    };
  }

  const { phone } = sanitizedQuery;
  try {
    await verifyToken(data.headers.authorization, phone);
  } catch (e) {
    return {
      statusCode: 403,
      payload: {
        message: 'Forbidden'
      }
    };
  }

  try {
    await updateUser(phone, sanitizedPayload);
    return {
      statusCode: 201,
      payload: {
        message: 'User successfully updated'
      }
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      payload: {
        message: 'Cannot create user'
      }
    };
  }
};

function sanitizeQuery ({ phone }) {
  return {
    phone: typeof phone === 'string' ? phone.trim() : false
  };
}

function sanitizePayload ({ firstName, lastName, password }) {
  return {
    firstName: typeof firstName === 'string' ? firstName.trim() : false,
    lastName: typeof lastName === 'string' ? lastName.trim() : false,
    password: typeof password === 'string' ? password.trim() : false
  };
}
