function validateQuery({ phone }) {
  const validationResults = [
    validatePhone(phone)
  ];

  const failureReasons = validationResults
    .filter((validation) => !validation.isValid)
    .map((validation) => validation.reason);
  if (failureReasons.length > 0) {
    return {
      isValid: failureReasons.length <= tolerableErrorsCount,
      reasons: failureReasons
    };
  }

  return {
    isValid: true
  };
}

function validateFields ({ firstName, lastName, password }, tolerableErrorsCount = 0) {
  const validationResults = [
    validateFirstName(firstName),
    validateLastName(lastName),
    validatePassword(password)
  ];

  const failureReasons = validationResults
    .filter((validation) => !validation.isValid)
    .map((validation) => validation.reason);
  if (failureReasons.length > 0) {
    return {
      isValid: failureReasons.length <= tolerableErrorsCount,
      reasons: failureReasons
    };
  }

  return {
    isValid: true
  };
}

function validateFirstName (firstName) {
  if (!firstName.length > 0) {
    return {
      isValid: false,
      reason: 'firstName cannot be empty'
    };
  }
  return {
    isValid: true
  };
}
function validateLastName (lastName) {
  if (!lastName.length > 0) {
    return {
      isValid: false,
      reason: 'lastName cannot be empty'
    };
  }
  return {
    isValid: true
  };
}
function validatePhone (phone) {
  if (!(phone.length === 10)) {
    return {
      isValid: false,
      reason: 'phone must have length of 10'
    };
  }
  return {
    isValid: true
  };
}

function validatePassword (password) {
  if (!password.length > 0) {
    return {
      isValid: false,
      reason: 'password cannot be empty'
    };
  }
  return {
    isValid: true
  };
}

module.exports = {
  validateFields, validateQuery
};
