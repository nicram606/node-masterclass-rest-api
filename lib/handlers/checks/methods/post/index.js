const validateFields = require('./validation');
const { getData } = require('../../../../auth');
const { getUserByPhone, createCheck, assignCheckToUser } = require('./data');
const { maxChecksPerUser } = require('../../../../../config');
const { uniqueString } = require('../../../../crypto');

// Required data: protocol (http | https), url, method, successCodes, timeoutSeconds
// Optional data: none
module.exports = async function postCheckHandler (data) {
  const sanitizedPayload = sanitizePayload(data.payload);

  if (Object.values(sanitizedPayload).some((fieldContent) => !fieldContent)) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid payload. Fields protocol (string), url (string), method (string), successCodes (array of integers), timeoutSeconds (number) are required.'
      }
    };
  }

  const { isValid, reasons } = validateFields(sanitizedPayload);
  if (!isValid) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid payload',
        reasons
      }
    };
  }

  if (!!data.headers.authorization) {
    return {
      statusCode: 401,
      payload: {
        message: 'Unauthenticated.'
      }
    }
  }

  let token;
  try {
    token = await getData(data.headers.authorization);
  } catch (e) {
    return {
      statusCode: 403,
      payload: {
        message: `Invalid or expired token.`
      }
    }
  }

  const { phone } = token;
  const userData = await getUserByPhone(phone);

  if ((userData.checks || []).length > maxChecksPerUser) {
    return {
      statusCode: 409,
      payload: {
        message: `User have defined maximum number of checks already: ${maxChecksPerUser}`
      }
    };
  }

  const checkId = uniqueString(20);
  try {
    await createCheck(checkId, phone, sanitizedPayload);
    await assignCheckToUser(checkId, userData);
    return {
      statusCode: 201,
      payload: {
        message: 'Check successfully created',
        check: {
          ...sanitizedPayload,
          checkId
        }
      }
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      payload: {
        message: 'Cannot create the check and/or assign it to the user'
      }
    };
  }
};

function sanitizePayload ({ protocol, url, method, successCodes, timeoutSeconds }) {
  return {
    protocol: typeof protocol === 'string' ? protocol.trim() : false,
    url: typeof url === 'string' ? url.trim() : false,
    method: typeof method === 'string' ? method.trim() : false,
    successCodes: Array.isArray(successCodes) ? successCodes.map((code) => typeof code !== 'number' ? parseInt(code, 10) : code) : false,
    timeoutSeconds: typeof timeoutSeconds === 'number' ? timeoutSeconds : false
  };
}
