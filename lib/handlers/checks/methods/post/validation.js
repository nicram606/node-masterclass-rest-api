const httpMethods = ['GET', 'POST', 'HEAD', 'PUT', 'DELETE', 'CONNECT', 'OPTIONS', 'TRACE', 'PATCH'];

function validateFields ({ protocol, url, method, successCodes, timeoutSeconds }) {
  const validationResults = [
    validateProtocol(protocol),
    validateUrl(url),
    validateMethod(method),
    validateSuccessCodes(successCodes),
    validateTimeoutSeconds(timeoutSeconds)
  ];

  const failureReasons = validationResults
    .filter((validation) => !validation.isValid)
    .map((validation) => validation.reason);
  if (failureReasons.length > 0) {
    return {
      isValid: false,
      reasons: failureReasons
    };
  }

  return {
    isValid: true
  };
}

function validateProtocol (protocol) {
  if (!['http', 'https'].includes(protocol)) {
    return {
      isValid: false,
      reason: 'protocol must be http or https'
    };
  }
  return {
    isValid: true
  };
}
function validateUrl (url) {
  if (!url.length > 3) {
    return {
      isValid: false,
      reason: 'validateUrl must be longer than 3 characters'
    };
  }
  return {
    isValid: true
  };
}
function validateMethod (method) {
  if (!httpMethods.includes(method)) {
    return {
      isValid: false,
      reason: `method must be one of ${httpMethods.join(', ')}`
    };
  }
  return {
    isValid: true
  };
}
function validateSuccessCodes (successCodes) {
  if (!successCodes.length > 0) {
    return {
      isValid: false,
      reason: 'successCodes cannot be empty'
    };
  }

  if (successCodes.any((code) => code < 100 || code > 1000)) {
    return {
      isValid: false,
      reason: 'each of successCodes values must be > 100 && < 1000'
    }
  }

  return {
    isValid: true
  };
}
function validateTimeoutSeconds (timeoutSeconds) {
  if (timeoutSeconds <= 0 || timeoutSeconds > 5) {
    return {
      isValid: false,
      reason: 'timeoutSeconds must be greater than zero and lower than 5'
    };
  }
  return {
    isValid: true
  };
}

module.exports = validateFields;
