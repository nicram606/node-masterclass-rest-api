const { read, create, update } = require('../../../../data');
const usersCollection = 'users';
const collection = 'checks';

async function getUserByPhone (phone) {
  const user = await read(usersCollection, phone);
  delete user.hashedPassword;
  return user;
}

async function createCheck (checkId, phone, check) {
  const checkObject = { ...check, checkId };
  try {
    await create(collection, checkId, checkObject);
  } catch (e) {
    throw e;
  }
  return true;
}

async function assignCheckToUser (checkId, userData) {
  userData.checks = userData.checks ? [...userData.checks, checkId] : [checkId];
  try {
    await update(usersCollection, userData.phone, userData);
  } catch (e) {
    throw e;
  }
  return true;
}

module.exports = {
  getUserByPhone,
  createCheck,
  assignCheckToUser
};
