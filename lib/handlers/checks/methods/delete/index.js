const validateFields = require('./validation');
const { verifyToken } = require('../../../../auth');
const { deleteUserByPhone } = require('./data');

// Required data: (query) phone
// Optional data: none
module.exports = async function deleteUserHandler (data) {
  const sanitizedPayload = sanitizePayload(data.query);

  if (Object.values(sanitizedPayload).some((fieldContent) => !fieldContent)) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid payload. Phone (string) parameter is required.'
      }
    };
  }

  const { isValid, reasons } = validateFields(sanitizedPayload);
  if (!isValid) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid payload',
        reasons
      }
    };
  }

  const { phone } = sanitizedPayload;
  try {
    await verifyToken(data.headers.authorization, phone);
  } catch (e) {
    return {
      statusCode: 403,
      payload: {
        message: 'Forbidden'
      }
    };
  }

  try {
    try {
      await deleteUserByPhone(phone);
      return {
        statusCode: 200,
        payload: {
          message: 'User deleted'
        }
      };
    } catch (e) {
      console.error(e);
      return {
        statusCode: 404,
        payload: {
          message: 'User not found'
        }
      };
    }
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      payload: {
        message: 'Cannot create user'
      }
    };
  }
};

function sanitizePayload ({ phone }) {
  return {
    phone: typeof phone === 'string' ? phone.trim() : false
  };
}
