'use strict';

const validateFields = require('./validation');
const { deleteToken } = require('./data');

// Required data: (query) tokenId
// Optional data: none
module.exports = async function deleteTokenHandler (data) {
  const sanitizedQuery = sanitizeQuery(data.query);

  if (Object.values(sanitizedQuery).some((fieldContent) => !fieldContent)) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid query. Phone (string) parameter is required.'
      }
    };
  }

  const { isValid, reasons } = validateFields(sanitizedQuery);
  if (!isValid) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid query',
        reasons
      }
    };
  }

  const { tokenId } = sanitizedQuery;
  try {
    await deleteToken(tokenId);
    return {
      statusCode: 200,
      payload: {
        message: 'Token deleted'
      }
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 400,
      payload: {
        message: 'Cannot delete the token'
      }
    };
  }
};

function sanitizeQuery ({ tokenId }) {
  return {
    tokenId: typeof tokenId === 'string' ? tokenId.trim() : false
  };
}
