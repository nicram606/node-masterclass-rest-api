const data = require('../../../../data');
const collection = 'tokens';

async function deleteToken (tokenId) {
  await data.delete(collection, tokenId);
  return true;
}

module.exports = {
  deleteToken
};
