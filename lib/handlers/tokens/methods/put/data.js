const { read, update } = require('../../../../data');
const collection = 'tokens';

async function findTokenById (tokenId) {
  try {
    const token = await read(collection, tokenId);
    return token;
  } catch (e) {
    return null;
  }
}

async function extendToken ({ id, phone }, expires = Date.now() + 1000 * 60 * 60) {
  const newToken = {
    id,
    phone,
    expires
  };

  await update(collection, id, newToken);
  return true;
}

module.exports = {
  findTokenById,
  extendToken
};
