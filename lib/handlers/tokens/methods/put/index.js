const { validateFields, validateQuery } = require('./validation');
const { findTokenById, extendToken } = require('./data');

// Required data: (query) tokenId, shouldExtend
// Optional data: none
module.exports = async function putTokenHandler (data) {
  const sanitizedPayload = sanitizePayload(data.payload);
  const validFields = Object.values(sanitizedPayload).filter((fieldContent) => fieldContent);
  if (validFields.length === 0) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid payload. Field shouldExtend (bool) is required.'
      }
    };
  }

  const { isValid, reasons } = validateFields(sanitizedPayload);
  if (!isValid) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid payload',
        reasons
      }
    };
  }

  const sanitizedQuery = sanitizeQuery(data.query);
  if (Object.values(sanitizedQuery).some((fieldContent) => !fieldContent)) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid query. Field tokenId (string) is required.'
      }
    };
  }

  const queryValidation = validateQuery(sanitizedQuery);
  if (!queryValidation.isValid) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid query',
        reasons: queryValidation.reasons
      }
    };
  }

  const { tokenId } = sanitizedQuery;
  const token = await findTokenById(tokenId);
  if (!token) {
    return {
      statusCode: 403,
      payload: {
        message: 'Cannot extend invalid token'
      }
    };
  }

  if (token.expires <= Date.now()) {
    return {
      statusCode: 401,
      payload: {
        message: 'Token is already expired'
      }
    };
  }

  try {
    await extendToken(token);
    return {
      statusCode: 201,
      payload: {
        message: 'Token successfuly extended'
      }
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      payload: {
        message: 'Cannot extend the token'
      }
    };
  }
};

function sanitizeQuery ({ tokenId }) {
  return {
    tokenId: typeof tokenId === 'string' ? tokenId.trim() : false
  };
}

function sanitizePayload ({ shouldExtend }) {
  return {
    shouldExtend: typeof shouldExtend === 'boolean' ? shouldExtend : false
  };
}
