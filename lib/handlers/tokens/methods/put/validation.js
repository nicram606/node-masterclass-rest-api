function validateQuery ({ tokenId }) {
  const validationResults = [
    validateTokenId(tokenId)
  ];

  const failureReasons = validationResults
    .filter((validation) => !validation.isValid)
    .map((validation) => validation.reason);
  if (failureReasons.length > 0) {
    return {
      isValid: failureReasons.length <= 0,
      reasons: failureReasons
    };
  }

  return {
    isValid: true
  };
}

function validateFields ({ shouldExtend }, tolerableErrorsCount = 0) {
  const validationResults = [
    validateShouldExtend(shouldExtend)
  ];

  const failureReasons = validationResults
    .filter((validation) => !validation.isValid)
    .map((validation) => validation.reason);
  if (failureReasons.length > 0) {
    return {
      isValid: failureReasons.length <= tolerableErrorsCount,
      reasons: failureReasons
    };
  }

  return {
    isValid: true
  };
}

function validateTokenId (tokenId) {
  if (tokenId.length !== 32) {
    return {
      isValid: false,
      reason: 'tokenId must have length of 32'
    };
  }
  return {
    isValid: true
  };
}
function validateShouldExtend (shouldExtend) {
  if (!shouldExtend) {
    return {
      isValid: false,
      reason: 'shouldExtend must be true to make this API call'
    };
  }
  return {
    isValid: true
  };
}

module.exports = {
  validateFields, validateQuery
};
