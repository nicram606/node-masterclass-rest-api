function validateFields ({ phone, password }) {
  const validationResults = [
    validatePhone(phone),
    validatePassword(password)
  ];

  const failureReasons = validationResults
    .filter((validation) => !validation.isValid)
    .map((validation) => validation.reason);
  if (failureReasons.length > 0) {
    return {
      isValid: false,
      reasons: failureReasons
    };
  }

  return {
    isValid: true
  };
}

function validatePhone (phone) {
  if (!(phone.length === 10)) {
    return {
      isValid: false,
      reason: 'phone must have length of 10'
    };
  }
  return {
    isValid: true
  };
}

function validatePassword (password) {
  if (!password.length > 0) {
    return {
      isValid: false,
      reason: 'password cannot be empty'
    };
  }
  return {
    isValid: true
  };
}

module.exports = validateFields;
