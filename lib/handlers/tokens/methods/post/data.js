const { hash, uniqueString } = require('../../../../crypto');
const { read, create } = require('../../../../data');
const usersCollection = 'users';
const collection = 'tokens';

async function findUser (phone) {
  try {
    const user = await read(usersCollection, phone);
    return user;
  } catch (e) {
    return null;
  }
}

function hashPassword (password) {
  return hash(password);
}

async function comparePasswords (hashedPassword, password) {
  const hPassword = hashPassword(password);

  return hPassword === hashedPassword;
}

async function createToken (phone, expires = Date.now() + 1000 * 60 * 60) {
  const token = {
    phone,
    expires,
    id: uniqueString()
  };
  await create(collection, token.id, token);
  return token;
}

module.exports = {
  findUser,
  comparePasswords,
  createToken
};
