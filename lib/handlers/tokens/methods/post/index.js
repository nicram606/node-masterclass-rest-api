const validateFields = require('./validation');
const { findUser, comparePasswords, createToken } = require('./data');

// Required data: phone, password
// Optional data: none
module.exports = async function postTokenHandler (data) {
  const sanitizedPayload = sanitizePayload(data.payload);

  if (Object.values(sanitizedPayload).some((fieldContent) => !fieldContent)) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid payload. Fields phone (string), password (string) are required.'
      }
    };
  }

  const { isValid, reasons } = validateFields(sanitizedPayload);
  if (!isValid) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid payload',
        reasons
      }
    };
  }

  const { phone } = sanitizedPayload;
  const user = await findUser(phone);
  if (!user) {
    return {
      statusCode: 404,
      payload: {
        message: 'User does not exist.'
      }
    };
  }

  const { password } = sanitizedPayload;
  const isValidPassword = await comparePasswords(user.hashedPassword, password);

  if (!isValidPassword) {
    return {
      statusCode: 403,
      payload: {
        message: 'Incorrect username or password'
      }
    };
  }

  try {
    const token = await createToken(phone);
    return {
      statusCode: 201,
      payload: {
        message: 'Token issued',
        token
      }
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      payload: {
        message: 'Cannot issue a token'
      }
    };
  }
};

function sanitizePayload ({ phone, password }) {
  return {
    phone: typeof phone === 'string' ? phone.trim() : false,
    password: typeof password === 'string' ? password.trim() : false
  };
}
