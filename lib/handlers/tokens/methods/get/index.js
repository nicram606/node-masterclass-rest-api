const validateFields = require('./validation');
const { getTokenById } = require('./data');

// Required data: (query) tokenId
// Optional data: none
module.exports = async function getTokenHandler (data) {
  const sanitizedQuery = sanitizeQuery(data.query);

  if (Object.values(sanitizedQuery).some((fieldContent) => !fieldContent)) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid query. tokenId (string) parameter is required.'
      }
    };
  }

  const { isValid, reasons } = validateFields(sanitizedQuery);
  if (!isValid) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid query',
        reasons
      }
    };
  }

  const { tokenId } = sanitizedQuery;
  try {
    const token = await getTokenById(tokenId);
    return {
      statusCode: 200,
      payload: {
        token
      }
    };
  } catch (e) {
    return {
      statusCode: 403,
      payload: {
        message: 'Invalid token id'
      }
    };
  }
};

function sanitizeQuery ({ tokenId }) {
  return {
    tokenId: typeof tokenId === 'string' ? tokenId.trim() : false
  };
}
