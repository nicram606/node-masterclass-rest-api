const { read } = require('../../../../data');
const collection = 'tokens';

async function getTokenById (tokenId) {
  const token = await read(collection, tokenId);
  return token;
}

module.exports = {
  getTokenById
};
