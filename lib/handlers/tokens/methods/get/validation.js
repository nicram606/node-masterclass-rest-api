function validateFields ({ tokenId }) {
  const validationResults = [
    validateTokenId(tokenId)
  ];

  const failureReasons = validationResults
    .filter((validation) => !validation.isValid)
    .map((validation) => validation.reason);
  if (failureReasons.length > 0) {
    return {
      isValid: false,
      reasons: failureReasons
    };
  }

  return {
    isValid: true
  };
}

function validateTokenId (tokenId) {
  if (tokenId.length !== 32) {
    return {
      isValid: false,
      reason: 'tokenId must have length of 32'
    };
  }
  return {
    isValid: true
  };
}

module.exports = validateFields;
