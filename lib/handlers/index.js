// Define request handlers
const handlers = {
  sample: async (data) => {
    // Return a http status code and a payload object
    return {
      statusCode: 200,
      payload: {
        message: 'sample handler',
        path: data.path
      }
    };
  },
  notFound: async (_) => {
    return {
      statusCode: 404,
      payload: {
        message: 'Not found'
      }
    };
  },
  users: require('./users'),
  tokens: require('./tokens'),
  checks: require('./checks'),
};

module.exports = handlers;
