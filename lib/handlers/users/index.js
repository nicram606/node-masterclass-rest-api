const subHandlers = {
  post: require('./methods/post'),
  get: require('./methods/get'),
  put: require('./methods/put'),
  delete: require('./methods/delete')
};

async function usersHandler (data) {
  const subHandler = subHandlers[data.method];
  if (!subHandlers) {
    return {
      statusCode: 405,
      payload: {
        name: 'Not found'
      }
    };
  }

  return subHandler(data);
}

module.exports = usersHandler;
