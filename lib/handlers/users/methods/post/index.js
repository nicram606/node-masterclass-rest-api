const validateFields = require('./validation');
const { verifyToken } = require('../../../../auth');
const { doesPhoneAlreadyExist, createUser } = require('./data');

// Required data: firstName, lastName, phone, password, tosAgreement
// Optional data: none
module.exports = async function postUserHandler (data) {
  const sanitizedPayload = sanitizePayload(data.payload);

  if (Object.values(sanitizedPayload).some((fieldContent) => !fieldContent)) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid payload. Fields firstName (string), lastName (string), phone (string), password (string), tosAgreement (bool) are required.'
      }
    };
  }

  const { isValid, reasons } = validateFields(sanitizedPayload);
  if (!isValid) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid payload',
        reasons
      }
    };
  }

  const { phone } = sanitizedPayload;
  if (await doesPhoneAlreadyExist(phone)) {
    return {
      statusCode: 409,
      payload: {
        message: `User with the phone number ${phone} already exists.`
      }
    };
  }

  try {
    await createUser(sanitizedPayload);
    return {
      statusCode: 201,
      payload: {
        message: 'User successfully created'
      }
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      payload: {
        message: 'Cannot create user'
      }
    };
  }
};

function sanitizePayload ({ firstName, lastName, phone, password, tosAgreement }) {
  return {
    firstName: typeof firstName === 'string' ? firstName.trim() : false,
    lastName: typeof lastName === 'string' ? lastName.trim() : false,
    phone: typeof phone === 'string' ? phone.trim() : false,
    password: typeof password === 'string' ? password.trim() : false,
    tosAgreement: typeof tosAgreement === 'boolean'
  };
}
