const { hash } = require('../../../../crypto');
const { read, create } = require('../../../../data');
const collection = 'users';

async function doesPhoneAlreadyExist (phone) {
  try {
    await read(collection, phone);
    return true;
  } catch (e) {
    return false;
  }
}

function hashPassword (password) {
  return hash(password);
}

async function createUser (userData) {
  const hashedPassword = hashPassword(userData.password);

  const newUser = {
    ...userData,
    hashedPassword
  };
  delete newUser.password;

  await create(collection, userData.phone, newUser);
}

module.exports = {
  doesPhoneAlreadyExist,
  createUser
};
