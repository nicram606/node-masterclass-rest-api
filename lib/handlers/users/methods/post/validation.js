function validateFields ({ firstName, lastName, phone, password, tosAgreement }) {
  const validationResults = [
    validateFirstName(firstName),
    validateLastName(lastName),
    validatePhone(phone),
    validatePassword(password),
    validateTosAgreement(tosAgreement)
  ];

  const failureReasons = validationResults
    .filter((validation) => !validation.isValid)
    .map((validation) => validation.reason);
  if (failureReasons.length > 0) {
    return {
      isValid: false,
      reasons: failureReasons
    };
  }

  return {
    isValid: true
  };
}

function validateFirstName (firstName) {
  if (!firstName.length > 0) {
    return {
      isValid: false,
      reason: 'firstName cannot be empty'
    };
  }
  return {
    isValid: true
  };
}
function validateLastName (lastName) {
  if (!lastName.length > 0) {
    return {
      isValid: false,
      reason: 'lastName cannot be empty'
    };
  }
  return {
    isValid: true
  };
}
function validatePhone (phone) {
  if (!(phone.length === 10)) {
    return {
      isValid: false,
      reason: 'phone must have length of 10'
    };
  }
  return {
    isValid: true
  };
}
function validatePassword (password) {
  if (!password.length > 0) {
    return {
      isValid: false,
      reason: 'password cannot be empty'
    };
  }
  return {
    isValid: true
  };
}
function validateTosAgreement (tosAgreement) {
  if (!tosAgreement) {
    return {
      isValid: false,
      reason: 'tosAgreement cannot be true'
    };
  }
  return {
    isValid: true
  };
}

module.exports = validateFields;
