const data = require('../../../../data');
const collection = 'users';

async function deleteUserByPhone (phone) {
  await data.delete(collection, phone);
  return true;
}

module.exports = {
  deleteUserByPhone
};
