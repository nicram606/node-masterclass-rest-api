const { hash } = require('../../../../crypto');
const { read, update } = require('../../../../data');
const collection = 'users';

async function updateUser (phone, updateData) {
  const user = await read(collection, phone);

  const newUser = {
    ...user
  };

  for (const key of Object.keys(updateData)) {
    if (updateData[key]) {
      newUser[key] = updateData[key];
    }
  }

  if (updateData.password) {
    const hashedPassword = hashPassword(updateData.password);
    newUser.hashedPassword = hashedPassword;
    delete newUser.password;
  }

  await update(collection, phone, newUser);
  return true;
}

function hashPassword (password) {
  return hash(password);
}

module.exports = {
  updateUser
};
