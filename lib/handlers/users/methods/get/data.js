const { read } = require('../../../../data');
const collection = 'users';

async function getUserByPhone (phone) {
  const user = await read(collection, phone);
  delete user.hashedPassword;
  return user;
}

module.exports = {
  getUserByPhone
};
