function validateFields ({ phone }) {
  const validationResults = [
    validatePhone(phone),
  ];

  const failureReasons = validationResults
    .filter((validation) => !validation.isValid)
    .map((validation) => validation.reason);
  if (failureReasons.length > 0) {
    return {
      isValid: false,
      reasons: failureReasons
    };
  }

  return {
    isValid: true
  };
}

function validatePhone (phone) {
  if (!(phone.length === 10)) {
    return {
      isValid: false,
      reason: 'phone must have length of 10'
    };
  }
  return {
    isValid: true
  };
}

module.exports = validateFields;
