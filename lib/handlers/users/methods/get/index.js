const validateFields = require('./validation');
const { verifyToken } = require('../../../../auth');
const { getUserByPhone } = require('./data');

// Required data: (query) phone
// Optional data: none
module.exports = async function getUserHandler (data) {
  const sanitizedQuery = sanitizeQuery(data.query);

  if (Object.values(sanitizedQuery).some((fieldContent) => !fieldContent)) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid query. Phone (string) parameter is required.'
      }
    };
  }

  const { isValid, reasons } = validateFields(sanitizedQuery);
  if (!isValid) {
    return {
      statusCode: 400,
      payload: {
        message: 'Invalid query',
        reasons
      }
    };
  }

  const { phone } = sanitizedQuery;
  try {
    await verifyToken(data.headers.authorization, phone);
  } catch (e) {
    return {
      statusCode: 403,
      payload: {
        message: 'Forbidden'
      }
    };
  }

  try {
    const user = await getUserByPhone(phone);
    return {
      statusCode: 200,
      payload: {
        user
      }
    };
  } catch (e) {
    return {
      statusCode: 404,
      payload: {
        message: 'User not found'
      }
    };
  }
};

function sanitizeQuery ({ phone }) {
  return {
    phone: typeof phone === 'string' ? phone.trim() : false
  };
}
