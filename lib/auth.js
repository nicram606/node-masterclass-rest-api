const { read } = require('./data');
const collection = 'tokens';

/**
 * Verify if a given token id is currently valid for a given user
 *
 * @param {String} tokenId
 * @param {String} phone
 * @returns {boolean} boolean indicating whether the token is valid or not
 */
async function verifyToken (tokenId, phone) {
  const token = await read(collection, tokenId.replace('Bearer ', ''));
  return token.phone === phone && token.expires > Date.now();
}

/**
 * Get user data by given token
 *
 * @param {String} tokenId
 * @param {String} phone
 * @returns {Object} data matching given token
 */
async function getData (tokenId) {
  const data = await read(collection, tokenId.replace('Bearer ', ''));
  return data && token.expires > Date.now();
}

module.exports = {
  verifyToken
};
