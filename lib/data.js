/**
 * Home for storing and editing data library
 */

// Dependencies
const fs = require('fs');
const path = require('path');
const { promisify } = require('util');

const openFile = promisify(fs.open);
const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);
const closeFile = promisify(fs.close);
const unlinkFile = promisify(fs.unlink);

async function truncateFile (fileDescriptor) {
  return new Promise((resolve, reject) => {
    fs.ftruncate(fileDescriptor, (error) => {
      if (error) {
        reject(error);
        return;
      }
      resolve(fileDescriptor);
    });
  });
}

function assembleFilepath (directory, filename) {
  return path.join(lib.baseDirectory, directory, `${filename}.json`);
}

function createError (message, error) {
  error.message = message;
  error.stack = (new Error()).stack;
  return error;
}

// Container for the module
const lib = {
  // Base directory of the data folder
  baseDirectory: path.join(__dirname, '../.data'),
  // Write data to a file
  create: async (directory = '', filename = '', data = '{}') => {
    // Assemble the target file path
    const filepath = assembleFilepath(directory, filename);
    try {
      // Open the file to write in
      const file = await openFile(filepath, 'wx');

      // Convert data to string
      const stringData = JSON.stringify(data);

      // Write to file and close it
      try {
        await writeFile(file, stringData);
        try {
          await closeFile(file);
          return true;
        } catch (e) {
          throw createError(`Cannot close the ${filepath}`, e);
        }
      } catch (e) {
        throw createError(`Cannot write to ${filepath}`, e);
      }
    } catch (e) {
      throw createError(`Cannot open the ${filepath}, it may already exist`, e);
    }
  },
  // Read data from a file
  read: async (directory = '', filename = '') => {
    // Assemble the target file path
    const filepath = assembleFilepath(directory, filename);
    try {
      const data = await readFile(filepath, 'utf8');
      return JSON.parse(data);
    } catch (e) {
      throw createError(`Cannot read from ${filepath}`, e);
    }
  },
  // Read data in an existing file
  update: async (directory = '', filename = '', data = '{}') => {
    // Assemble the target file path
    const filepath = assembleFilepath(directory, filename);
    try {
      // Open the file to write in
      const file = await openFile(filepath, 'r+');

      // Convert data to string
      const stringData = JSON.stringify(data);

      try {
        await truncateFile(file);
        // Write to the file and close it
        try {
          await writeFile(file, stringData);
          try {
            await closeFile(file);
            return true;
          } catch (e) {
            throw createError(`Cannot close the ${filepath}`, e);
          }
        } catch (e) {
          throw createError(`Cannot write to ${filepath}`, e);
        }
      } catch (e) {
        throw createError(`Cannot truncate the ${filepath}. ${e}`, e);
      }
    } catch (e) {
      throw createError(`Cannot open the ${filepath}. ${e}`, e);
    }
  },
  // Delete a file
  delete: async (directory = '', filename = '') => {
    // Assemble the target file path
    const filepath = assembleFilepath(directory, filename);
    // Unlink the file
    try {
      await unlinkFile(filepath);
      return true;
    } catch (e) {
      throw createError(`Cannot unlink the ${filepath}. ${e}`, e);
    }
  }
};

// Export the module
module.exports = lib;
